import sqlite3

connection = sqlite3.connect('puszkaPandory.db')


with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO projects (name, techs, link) VALUES (?, ?, ?)",
            ('Portfolio', 'python js css sqlite', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ')
            )

connection.commit()
connection.close()