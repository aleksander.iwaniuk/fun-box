from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort
import sqlite3

def get_db_connection():
    conn = sqlite3.connect('puszkaPandory.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_project(project_id):
    conn = get_db_connection()
    project = conn.execute('SELECT * FROM projects WHERE id = ?',
                        (project_id,)).fetchone()
    conn.close()
    if project is None:
        abort(404)
    return project

app = Flask(__name__)
app.config['SECRET_KEY'] = 'kriswherethefuckarewe'


@app.route('/')
def index():
    conn = get_db_connection()
    projects = conn.execute('SELECT * FROM projects').fetchall()
    conn.close()
    return render_template('index.html', projects=projects)

@app.route('/admin')
def admin():
    return render_template('admin.html')

@app.route('/addproject', methods=('GET', 'POST'))
def addproject():
    if request.method == "POST":
        name = request.form['name']
        techs = request.form['techs']
        link = request.form['link']

        if not name or not techs or not link:
            flash("Missing data")
        else:
            conn = get_db_connection()
            conn.execute("INSERT INTO projects (name, techs, link) VALUES (?, ?, ?)",
                        (name, techs, link))
            conn.commit()
            conn.close()
            return redirect(url_for('admin'))

    return render_template('create_project.html')

@app.route('/<int:id>/edit', methods=('GET', 'POST'))
def edit(id):
    project = get_project(id)

    if request.method == 'POST':
        name = request.form['name']
        techs = request.form['techs']
        link = request.form['link']

        if not name or not techs or not link:
            flash('Missing data')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE projects SET name = ?, techs = ?, link = ?'
                         ' WHERE id = ?',
                         (name, techs, link, id))
            conn.commit()
            conn.close()
            flash('Changed - {}'.format(project['name']))
            return redirect(url_for('admin'))

    return render_template('edit.html', project=project)

@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    project = get_project(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM projects WHERE id = ?', (id,))
    conn.commit()
    conn.close()
    flash('usunięto {}'.format(project['name']))
    return redirect(url_for('admin'))

@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.run()
